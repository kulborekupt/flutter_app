// ignore: prefer_const_constructors
// ignore_for_file: use_key_in_widget_constructors
// ignore: unnecessary_new
// ignore: prefer_const_constructors

import 'package:flutter/material.dart';
import 'MoneyBox.dart';
import 'package:http/http.dart' as http;
import 'Product.dart';
import 'dart:math';

void main() {
  var main = MyApp();
  runApp(main);
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "MyApp",
      home: MyHomePage(),
      theme: ThemeData(primarySwatch: Colors.brown),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    print("BUITE");

    List<Widget> moneyBox = createBox();

    return Scaffold(
      backgroundColor: Colors.grey[250],
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: const Text("Menu"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView.builder(
          itemCount: moneyBox.length,
          itemBuilder: (BuildContext context, int index) {
            return moneyBox[index];
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => {
          setState(() {
            print("set");
          })
        },
        child: Icon(Icons.place),
      ),
    );
  }

  List<Widget> createBox() {
    List<Widget> moneyBoxes = [];
    // Create MoneyBox objects
    var siezBox = SizedBox(height: 10);
    for (int i = 0; i < 30; i++) {
      MoneyBox box1 = MoneyBox(getRandomString(10), 1000.25, Colors.blue, 120);
      moneyBoxes.add(box1);

      moneyBoxes.add(siezBox);
    }
    // MoneyBox box1 = MoneyBox("Savings", 1000.25, Colors.blue, 120);
    // MoneyBox box2 = MoneyBox("Emergency Fund", 9000, Colors.red, 150);

    // Add the objects to the list
    // moneyBoxes.add(box1);
    // moneyBoxes.add(box2);

    return moneyBoxes;
  }

  @override
  void initState() {
    super.initState();
    gitExchangeRate();
    print("INIT STATE");
  }

  @override
  void dispose() {
    print("DISPOSE");
    super.dispose();
  }

  Future<void> gitExchangeRate() async {
    var url = "https://dummyjson.com/products";
    var uri = Uri.parse(url);
    var response = await http.get(uri);
    print(response.body);
  }

  var _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  Random _rnd = Random();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
}
