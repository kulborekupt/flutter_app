import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MoneyBox extends StatelessWidget {
  String title;
  Color color;
  double amount;
  double size;

  MoneyBox(this.title, this.amount, this.color, this.size);

  String getAmount() {
    var result = NumberFormat("#,###.##").format(this.amount);
    print(this.amount);
    print(result);
    return result.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        boxShadow: [BoxShadow(blurRadius: 2)],
        color: color,
        borderRadius: BorderRadius.circular(20),
      ),
      height: size,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            title.toString(),
            style: TextStyle(color: Colors.white, fontSize: 35),
          ),
          Expanded(
            child: Text(
              getAmount(),
              style: TextStyle(color: Colors.white, fontSize: 35),
              textAlign: TextAlign.right,
            ),
          ),
        ],
      ),
    );
  }
}
