// ignore: prefer_const_constructors
// ignore_for_file: use_key_in_widget_constructors
// ignore: unnecessary_new
// ignore: prefer_const_constructors

import 'package:flutter/material.dart';

void main() {
  var main = MyApp();
  runApp(main);
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "MyApp",
      home: MyHomePage(),
      theme: ThemeData(primarySwatch: Colors.brown),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int countNumber = 1; // สร้าง stage
  String textShow = "Hello World";
  String imageName =
      "https://cdn.pixabay.com/photo/2023/11/26/19/09/butterfly-8414148_1280.jpg";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Sert App"),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Image(image: NetworkImage(imageName)),
            Text(textShow.toString()),
            Text(
              countNumber.toString(),
              style: TextStyle(fontSize: 60),
            ),
          ], // Children
        ),
        floatingActionButton: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            FloatingActionButton(
              onPressed: addFunction,
              child: Icon(Icons.add),
            ),
            SizedBox(height: 16),
            FloatingActionButton(
              onPressed: resetNumber,
              child: Icon(Icons.remove),
            ),
          ],
        ));
  }

  void addFunction() {
    setState(() {
      countNumber++;
      showTextWhenCountTen();
    });
  }

  void resetNumber() {
    setState(() {
      countNumber = 0;
      imageName =
          "https://cdn.pixabay.com/photo/2023/11/26/19/09/butterfly-8414148_1280.jpg";
    });
  }

  void showTextWhenCountTen() {
    if (countNumber == 10) {
      setState(() {
        textShow = "TEN";
        imageName =
            "https://media.istockphoto.com/id/960539620/th/%E0%B8%A3%E0%B8%B9%E0%B8%9B%E0%B8%96%E0%B9%88%E0%B8%B2%E0%B8%A2/%E0%B8%A0%E0%B8%B2%E0%B8%9E%E0%B8%A3%E0%B8%B0%E0%B8%A2%E0%B8%B0%E0%B9%83%E0%B8%81%E0%B8%A5%E0%B9%89%E0%B8%82%E0%B8%AD%E0%B8%87%E0%B8%AB%E0%B8%B2%E0%B8%87%E0%B8%99%E0%B8%81%E0%B8%99%E0%B8%B2%E0%B8%87%E0%B9%81%E0%B8%AD%E0%B9%88%E0%B8%99%E0%B8%88%E0%B8%B8%E0%B8%94%E0%B8%AA%E0%B8%B5%E0%B9%81%E0%B8%94%E0%B8%87.jpg?s=2048x2048&w=is&k=20&c=n0DXrGG2SZ2mAaCbFllKwcdPgrFvwBhoaORjKlR80lo=";
      });
    }
  }
}
